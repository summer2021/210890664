#### 中期报告

##### 进展

- 增加Vineyard中C++部分对基础数据结构Date32和Date64的支持，增加在Array和table_shuffler中对这两个Date数据结构的支持。

##### 开发过程

- 开始思路：
    - 考虑输入输出的情况，输入为string类型的“dd-mm-yyyy HH:mm:ss”的格式，输出为 vineyard所要的类型，这样基础的数据结构用于服务其他较为复杂的数据结构。
- 实际：
    - 仔细阅读了arrow和vineyard部分代码，注意到vineyard会直接调用arrow中已实现好的数据结构进行使用，于是直接选择使用arrow中已经实现好的date32和date64的数据结构，作为底层数据结构进行调用。

##### 后续

- 对于arrow中有的数据结构，应将arrow实现的部分作为底层逻辑调用。
- 参考arrow中的test部分完善测试信息。
- 参考其他库实现设计map，dictionary等。

